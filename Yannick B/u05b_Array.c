/*Yannick Borschneck - u05a_Array*/
/*21.11.2017 - Versch. Bibliotheksfunktionen mit Arrays als Übungsaufgabe*/

#include <stdio.h>

char userEingabe(char eingabe[], int bufsiz) {
	char c;
	int i = 0;

	do {
		c = getchar();
		if (c >= '0' && c <= '9') {							
			eingabe[i] = c;
			i++;
		}

	} while (c != EOF && eingabe[i] <=  bufsiz - 1);
	return c;
}


int main() {
	char zahlenArray[10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000];

	zahlenArray[0] = userEingabe(zahlenArray,10);
	
	printf("Das steht drin: %c \n", zahlenArray[0]);

	return 0;
}