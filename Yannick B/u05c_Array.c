/*Yannick Borschneck - 28.11.12 */
/*Palindromerkennung von einer Eingabe*/

#include <stdio.h>
#include <stdbool.h>

bool paliCheck(char wortEingabe[]){													/* Überprüft ob eine Worteingabe ein Palindrom ist */
	int wordlength = 0;
	int wordstart = 0;
	
	while(wortEingabe[wordlength] != '\0'){											/* Länge des Wortes feststellen */
		wordlength++;
	}
	
	wordlength = wordlength - 1;													/* Damit kürzen wir das Enter weg! */
	
	while(wortEingabe[wordstart] == wortEingabe[wordlength] && wordlength >= 0){	/* Gegenlaufen lassen solange alles gleich ist */
		wordstart++;
		wordlength--;
		
		return true;
	}
	return false;
}
char * rot13(char userEingabe[], char Ausgabe[]){					/* Führt die Caesar - Verschlüsselung durch */
	
	int index = 0;
	
	while(userEingabe[index] != '\0'){		
	if (userEingabe[index] >= 'a' && userEingabe[index] <= 'm'){	/* Buchstabenrotation */
		Ausgabe[index] = userEingabe[index] + 13;
	}
	else {
		Ausgabe[index] = userEingabe[index] - 13;
	}
	index++;														/* Nächsten Buchstaben rotieren */
	}
	
	return Ausgabe;
	
}


int main(){
	char userEingabe[1024];
	char rot13Eingabe[1024];
	char rot13Ausgabe[1024];
		
		scanf("%s", userEingabe);
		paliCheck(userEingabe);
		
		if (paliCheck(userEingabe)){								/* Ergebnisausgabe für paliCheck */
			printf("Das Wort ist ein Palindrom!\n");
		}
		else {
			printf("Das Wort ist kein Palindrom!\n");
		}
	
		printf("Jetzt gib ein Wort zum Verschlüsseln ein!: ");
		scanf("%s", rot13Eingabe);
		
		rot13(rot13Eingabe, rot13Ausgabe);
		printf("Das ist die Verschlüsselung: %s \n", rot13Ausgabe);	/* Ergebnisausgabe für rot13 */
	
	return 0;
}