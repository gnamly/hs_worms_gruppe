// 10.01.2018
// string testing

#include <stdio.h>
#include <string.h>

int xstrnlen(char s[], int n){
	if (s[n] == '\0'){				// Nullbyte an stelle n suchen
		return strlen(s);			// Wenn gefunden, dann die Stringlaenge ausgeben
	}
	
	else { 							// Ansonsten Fehler ausgeben
		printf ("Fehler"); 
		return 0; 
	}
}

char * xstrncpy(char nach[], char von[], int n){
	if (von[n] == '\0'){			// Nullbyte an stelle n suchen
		return strcpy(nach,von);	// Wenn gefunden, dann Stringcopy ausfuehren
	}
	
	else {							// Ansonsten Fehler ausgeben
		printf ("Fehler");
		return 0;
	}
}

char * xstrncat(char nach[], char von[], int n){
	
	if (von[n] == '\0'){			// Nullbyte an stelle n suchen
		return strcat(nach,von);	// Wenn gefunden, dann Stringcat ausfuehren
	}
	
	else {							// Ansonsten Fehler ausgeben
		printf ("Fehler\n");
		return 0;
	}
	
	return 0;
}

int xstrncmp(char a[], char b[], int m){
	
	if (b[m] == '\0'){				// Nullbyte an stelle n suchen
		return strcmp(b,a);			// Wenn gefunden, dann die StringCompare ausgeben
	}
	
	else { 							// Ansonsten Fehler ausgeben
		printf ("Fehler"); 
		return 1; 
	}
}

int eingabe(char einlesWort[], int maxlen){
									// Eingabe des Wortes
	printf("Bitte das Wort zur Palindromerstellung eingeben: ");
	scanf("%s", einlesWort);
	
	return xstrnlen(einlesWort,maxlen);	
}

void palindrom(char einlesWort[], char palindromvar[]){
	printf("Ursprungswort: %s\n",einlesWort);
	palindromvar = einlesWort;
	char anhang[20];
	for(int i = 0;i <= strlen(einlesWort);i++)
	{
		anhang[i] = palindromvar[strlen(palindromvar)-i-1];
	}
	palindromvar = xstrncat(palindromvar, anhang, strlen(anhang));
	printf("Palindrom: %s\n", palindromvar);
}

int main (){
		
	char einlesWort[20] = "";
	char palindromvar[40] = "";
	const int maxlen=20;
	
	printf("Länge des Wortes: %d\n",eingabe(einlesWort,maxlen));
	palindrom(einlesWort,palindromvar);
	
	return 0;
}