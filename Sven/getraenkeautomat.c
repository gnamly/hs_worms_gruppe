//Getränkeautomat

#include <stdio.h>
#include <stdbool.h>


void  getraenkeauswahl(int * zsorte);
float rechnung(int * zanzahlflasche, int * zsorte);
int flaschenausgabe(int * zanzahlflasche);
bool falschgeldcheck(float einwurf);
int flaschenmenge();
void bezahlvorgang(float * zendbetrag, int * zanzahlflasche);

int main (){
	
	float endbetrag=0.0;
	int auswahl=0,anzahlflasche=0,sorte=0;
	
	//Pointer
	int *zsorte = &sorte;
	int *zanzahlflasche = &anzahlflasche;
	float *zendbetrag = &endbetrag;
		
	
	getraenkeauswahl(zsorte);
	anzahlflasche=flaschenmenge();
	endbetrag=rechnung(zanzahlflasche,zsorte);
	bezahlvorgang(zendbetrag,zanzahlflasche);
	

	
	return 0;
}



//Getraenkeauswahl
void getraenkeauswahl(int *zsorte){
	int auswahl=0; 
	printf("Waehlen Sie ihr Getraenk aus:\n");
	
	do{		
		printf("1) Wasser (0.5 Euro)\n2) Limonade (1.0 Euro)\n3) Bier (2.0 Euro)\n\nGeben Sie 1, 2, oder 3 ein: ");
		scanf("%d",&auswahl);
		//Auswahl der Sorte per switch case in schleife falls falscheingabe
		switch(auswahl){
			case 1: 	
				*zsorte=1; 						
				break;
			case 2: 	
				*zsorte=2; 						
				break;
			case 3: 	
				*zsorte=3;	 				  	
				break;
			default: 	
				printf("Fehler\n"); 		 	
				break;
		}
	}while(*zsorte <= 0); 
}


//Rechnung wird erstellt
float rechnung(int *zanzahlflasche, int *zsorte){
	float zuzahlen=0.0,einzelpreis=0.0;
	//Zuordnung des einzelpreises pro Flasche
	switch(*zsorte){
		case 1: 	einzelpreis=0.5; 					break;
		case 2: 	einzelpreis=1.0; 					break;
		case 3: 	einzelpreis=2.0;	 				break;
		default: 	printf("Fehler\n\n\n"); return 0;	break;
	}
	//Berechnung und rückgabe Gesamtpreis
	return zuzahlen=einzelpreis*(*zanzahlflasche);
}

//Getraenkeausgabe
int flaschenausgabe(int *zanzahlflasche){
	int i;
	for (i=1;i<=*zanzahlflasche;i++){
		printf("Flache %d von %d wurde ausgegeben\n",i,*zanzahlflasche);
	}
}

//Pruefung von Falschen Geldstuecken
bool falschgeldcheck(float einwurf){
	float muenzen[6] = { 0.05, 0.10, 0.20, 0.50, 1.0, 2.0 };		//definierte gültige Münzen
	int i;
	bool ret;
	
	//Prüfung ob eingworfene münze den definierten entspricht
	for (i=0; i<6; i++){
		if (einwurf == muenzen[i]){ 
			ret=false;
			break;
		}
		else {
			ret=true;
		}
	}
	return ret;
}

//Menge der Flaschen
int flaschenmenge(){
	int anzahlflasche;
	printf("Wieviele Flaschen duerfen es sein?: ");
	scanf("%d",&anzahlflasche);
	return anzahlflasche;
}

//Bezahlvorgang
void bezahlvorgang(float * zendbetrag, int *zanzahlflasche){
	float einwurf=0.0;
	bool abbruch=false;			//Hilfsvariable für Abbruch der bezahlung
	
	printf("\nKosten: %.2f euro",*zendbetrag);
	
	do{
		printf("\nMuenzeinwurf: ");
		scanf("%f",&einwurf);
		
		//Abbruch der bezahlung
		if (einwurf==0){
			printf("Abbruch! Sie erhalten das Eingeworfene Geld wieder\n");
			bool abbruch=true;
			break;
		}
		
		//Prüfung auf falsch eingeworfene Münze
		if (falschgeldcheck(einwurf)==1){
			printf("Falsches Geldstueck eingeworfen\n");
			printf("Es Fehlt noch %.2f euro\n",*zendbetrag);
		}
		else if (abbruch != true){
			*zendbetrag=(*zendbetrag)-einwurf;
			if (*zendbetrag!=0){
				if (*zendbetrag<=0){ 
					printf("\n\nSie erhalten noch Restgeld: %.2f euro\n",*zendbetrag);
					break;		
				}
				else{
					printf("Es Fehlt noch %.2f euro\n",*zendbetrag);
				}
			}
			else {
				printf("\n\nGetraenkeausgabe folgt.\n");
				flaschenausgabe(zanzahlflasche);
			}
		}
	}while(*zendbetrag!=0);
}