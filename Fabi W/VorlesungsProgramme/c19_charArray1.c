/* C-Demoprogramm fuer nullterminierte Zeichenketten */

#include <stdio.h>

int main() {
	char mybuffer [1024];		/* mein Arbeitspuffer fuer Zeichenketten */
	char c;				/* Eingabe-Zeichen */
	int index1 =0;			/* Subskriptionshilfe in das Array */

	printf("Bitte Zeichen eingeben bis EOF (STRG-D): ");

	do {
		c = getchar();		/* Zeichen einlesen */

		if (c == EOF) {
			break;
		}

		mybuffer [index1] = c;	/* Zeichen im Array ablegen */
		index1++;		/* "Index" der Subskription weiterschalten */

	} while (c != EOF && index1 < 1024);	/* PUFFERUEBERLAUF ABFANGEN! */

	/* nach der Schleife noch das terminierende Nulbyte setzen */

	mybuffer[index1] = '\0';

	printf("\nmybuffer: |%s|\n", mybuffer);

	return 0;
}
