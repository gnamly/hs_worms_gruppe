/* C-Demoprogramm fuer nullterminierte Zeichenketten */

#include <stdio.h>

int einlesen(char mybuff [], int bufsiz) {
					/* Einlesefunktion, gibt die Laenge */
					/* der Zeichenkette zurueck */
	char c;				/* Eingabe-Zeichen */
	int index1 =0;			/* Subskriptionshilfe in das Array */

	printf("Bitte Zeichen eingeben bis EOF (STRG-D): ");

	do {
		c = getchar();		/* Zeichen einlesen */

		if (c == EOF) {
			break;
		}

		mybuff [index1] = c;	/* Zeichen im Array ablegen */
		index1++;		/* "Index" der Subskription weiterschalten */

	} while (c != EOF && index1 < bufsiz-1);/* PUFFERUEBERLAUF ABFANGEN! */

	/* nach der Schleife noch das terminierende Nulbyte setzen */

	mybuff[index1] = '\0';

	return index1;
}

#define MYBUFFSIZ 1024

int main() {
	char mybuffer [MYBUFFSIZ];	/* mein Arbeitspuffer fuer Zeichenketten */
	int laenge;

	laenge = einlesen(mybuffer, MYBUFFSIZ);		/* Einlesefunktion aufrufen */

	printf("\nmybuffer: |%s| (laenge=%i)\n", mybuffer, laenge);

	return 0;
}
