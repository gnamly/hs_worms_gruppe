/* C-Demoprogramm fuer statische Variablen */


#include <stdio.h>

double mittelwert (int ein) {
	double mw = 0.0;	/* Variable fuer die Mittelwert-Berechnung */
	static int summe = 0;	/* ctatic-Variablen verlieren ihren Wert nicht */
	static int anzsummand = 0;
	
	summe += ein;		/* aktuellen Wert zur bisherigen Summe dazuaddieren */
	anzsummand++;		/* Anzahl der Summanden um 1 erhoehen */

	printf("Kontrollausgabe: summe=%i, anzsummand=%i\n", summe, anzsummand);

	mw = (double)summe/(double)anzsummand;	
				/* ACHTUNG! Integer-Division / Ganzzahl-Division */
				/* durch Cast zur Gleitkomma-Division gemacht */
	return mw;
}


int main() {
	int eingabe;
	double mittel = 0.0;		/* Initialisierung nur zur Sicherheit */

	do {
		printf("Bitte eine Ganzzahl eingeben: ");
		scanf("%i", &eingabe);

		if (eingabe == 0) {	/* Schleifenabbruch durch Benutzereingabe */
			break;
		}
		
		/* Mittelwertberechnung */

		mittel = mittelwert(eingabe);
		printf("Der Mittelwert ist: %lf\n", mittel);
	
	} while (eingabe != 0);		/* Sicherheit: Schleifenende doppelt sicher */

	return 0;
}
