﻿//------------------------------------------------------------------//
// Bildverarbeitung Basics mit WPF                                  //
// 26.3.2018 DS                                                     //
// 10.4.2018 DS Update save, format convert                         //
//------------------------------------------------------------------//
// das zedgraph Control ist in der Toolbox nicht sichtbar, weil     //
// es ein Forms Control ist !                                       //
// hinzunehmen wie in der Aufgabe beschrieben                       //
// Pixel lesen: alle Pixel in ein Arry kopieren : CopyPixel         //
// Pixel schreiben : das Array wieder ins Bild kopieren : WritePixel//
// Alternative: Pointer auf den BackBuffer holen und in diesem      //
// herumpointern !! Vorsicht !!!                                    //
//------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ZedGraph;
using System.IO;                    // zum File lesen/schreiben      
using Microsoft.Win32;              // hier sind die File Dialoge

namespace Basics_WPF_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage img = new BitmapImage();                        // das Bild, damit es von überall zugänglich ist
        FormatConvertedBitmap cimg = new FormatConvertedBitmap();   // das nach Bgr32 konvertierte Bild
        WriteableBitmap wbm = null;                                 // das Bild für das rechte image control

        public MainWindow()
        {
            InitializeComponent();

        }

        //
        // --- Bild speichern
        //
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            // wie ?   --- das Netz wusste alles ......

            // --- Save Dialog -------------
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "Bilder |*.jpg; *.bmp; *.png, .tif, .gif, .wmp";
            dlg.ShowDialog();

            if (dlg.FileName == "")
                return;

            // --- je nach Typ einen Encoder erzeugen
            BitmapEncoder enc = null;

            switch (dlg.FileName.ToLower().Split('.').Last())
            {
                case "png":
                    enc = new PngBitmapEncoder();
                    break;

                case "jpg":
                    enc = new JpegBitmapEncoder();                   
                    break;

                case "bmp":
                    enc = new BmpBitmapEncoder();
                    break;

                case "tif":
                    enc = new TiffBitmapEncoder();
                    break;

                case "gif":
                    enc = new GifBitmapEncoder();
                    break;

                case "wmp":
                    enc = new WmpBitmapEncoder();
                    break;
                default:
                    MessageBox.Show("Format nicht unterstützt");
                    return;             // --- kein unterstütztes Format gefunden
            }
  
            // --- das Bild zum Encoder hinzufügen    --- irgendwie sollte festgelegt werden, welches Bild gespeichert werden soll ....
            enc.Frames.Add(BitmapFrame.Create(wbm));

            // --- schreiben 
            using (Stream sw = new FileStream(dlg.FileName, FileMode.Create))
            {
                enc.Save(sw);
            }

        }

        //
        // --- neues Bild laden -- Histogramm berechnen -- sonst was machen
        //
        
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            // --- Dialog erzeugen 
            OpenFileDialog dlg = new OpenFileDialog();

            // --- das richtige Directory wählen -- krampfig! geht das einfacher ?
            string str = Directory.GetCurrentDirectory();       // hier sind wir
            string[] strtk = str.Split('\\');                   // den Pfad aufspalten

            string dir = "";                                    // neu zusammensetzen aber 3 Ebenene höher gehen
            for (int i = 0; i < strtk.Length - 3; i++)
                dir = dir + strtk[i] + "\\";

            dir += "Images\\";                                  // hier ist Images

            // --- sicherheitshalber prüfen, ob es das Verzeichnis gibt
            if (Directory.Exists(dir))
                dlg.InitialDirectory = dir;
            else
                dlg.InitialDirectory = str;

            // --- Dialog anzeigen
            dlg.Filter = "Bilder |*.jpg; *.bmp; *.png";
            dlg.ShowDialog();

            // --- Bild laden und anzeigen
            BitmapImage tmpimg = new BitmapImage();
            tmpimg.BeginInit();
            tmpimg.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            tmpimg.UriSource = new Uri(dlg.FileName, UriKind.RelativeOrAbsolute);
            tmpimg.EndInit();

            image_in.BeginInit();
            image_in.Source = tmpimg;
            image_in.EndInit();


            // --- das Bild wird nach Bgr32 konvertiert - in jedem Fall        !! hier hätte ich gerne cimg gehabt, aber er will das nicht, BeginInit() ist das Problem
            FormatConvertedBitmap cimgtmp = new FormatConvertedBitmap();
            cimgtmp.BeginInit();
            cimgtmp.Source = tmpimg;
            cimgtmp.DestinationFormat = PixelFormats.Bgr32;
            cimgtmp.EndInit();

            // --- Bilddaten ----------------
            //int Height = (int)(cimgtmp.Height * cimgtmp.DpiY / 96);      // Höhe holen und auf Pixel umrechnen
            //int Width = (int)(cimgtmp.Width * cimgtmp.DpiX / 96);        // Höhe holen und auf Pixel umrechnen
            int Height = cimgtmp.PixelHeight;
            int Width  = cimgtmp.PixelWidth;

            // --- Histogramm berechnen -----

            // --- Pixel aus dem Bild holen - in ein Bytearray passender Grösse speichern
            byte[] pix = new byte[Height * Width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 

            cimgtmp.CopyPixels(pix, Width * 4, 0);

            // --- Histogramm berechnen
            int[] hist = new int[256];

            // --- über alle Pixel weg
            for (int i = 0; i < Height * Width * 4; i += 4)
            {
                hist[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
            }

            // --- Histogramm ausgeben
            FillHisto(zedgraph, hist);
            zedgraph.Invalidate();
            zedgraph.Update();

            // --- das geladene Bild ins rechte image control schreiben
            // --- Writeable Bitmap erzeugen, so gross wie das gelesene convertierte Bild
            wbm = new WriteableBitmap(cimgtmp);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, Width, Height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, Width * 4, 0);

            image_out.Source = wbm;
        }

        //
        // --- exit -  nach save fragen !
        //
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //  --- save ? Message Box
            MessageBoxResult res =  MessageBox.Show(" möchten sie speichern ? ", "save", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (res != MessageBoxResult.No)
                Save_Click(null, new RoutedEventArgs());
                 
            Application.Current.Shutdown();
           
        }

        // 
        // --- wird aufgerufen, wenn das Window geladen ist, dann ist alles initialisiert
        //     hier wird nur deshalb ein Bild geladen, dass man am Anfang etwas sieht -> äbdern
        //
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // --- Startbild laden
            img.BeginInit();
            // --- absoluter Pfad: das Bild wird immer da gesucht und genommen, wenn es gefunden wird; Exception, falls nicht
            //img.UriSource = new Uri(@"G:\workdir\Bildverarbeitung\Basics_WPF_1\Images\lena 512.jpg", UriKind.RelativeOrAbsolute);
            // --- relativer Pfad: bezieht sich auf ein Verzeichnis, welches im Projekt existiert ! keine Exception, wenn das File nicht existiert
            img.UriSource = new Uri(@"..\..\Images\lena 512.jpg", UriKind.RelativeOrAbsolute);
            img.EndInit();


            image_in.Source = img;

            // --- Pixel Format  das ist wichtig !!  Lena ist BGRA
            PixelFormat fmt = img.Format;

            // --- das Bild wird nach Bgr32 konvertiert - in jedem Fall
            cimg.BeginInit();
            cimg.Source = img;
            cimg.DestinationFormat = PixelFormats.Bgr32;
            cimg.EndInit();

            // --- Bilddaten ----------------
            //int Height = (int)(cimg.Height * cimg.DpiY / 96);      // Höhe holen und auf Pixel umrechnen
            //int Width = (int)(cimg.Width * cimg.DpiX / 96);       // Höhe holen und auf Pixel umrechnen
            int Height = cimg.PixelHeight;
            int Width = cimg.PixelWidth;
 

            // --- Histogramm berechnen -----

            // --- Pixel aus dem Bild holen - in ein Bytearray passender Grösse speichern
            byte[] pix = new byte[Height * Width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 

            cimg.CopyPixels(pix, Width * 4, 0);
            
            // --- Histogramm berechnen
            int[] hist = new int[256];

            // --- über alle Pixel weg
            for (int i = 0; i < Height*Width * 4; i += 4)
            {
 
                hist[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
                //int g=(pix[i]+pix[i+1]+pix[i+2])/ 3;
                //hist[g]++;
                if(pix[i] == 70) {
                    pix[i + 0] = 0;
                    pix[i + 1] = 0;
                    pix[i + 2] = 255;
                }
                else
                {
                    pix[i + 0] = 0;
                    pix[i + 1] = 0;
                    pix[i + 2] = 0;
                }
            }

            //for(int j = 0; j < Height; j++)
            //    for (int i = 0; i < Width * 4; i += 4)
            //    {

            //        pix[i + j * Width*4] = (byte)(i/8);
            //        pix[i + j * Width*4 + 1] =(byte) (i/8);
            //        pix[i + j * Width*4 + 2] = (byte)(i/8);
            //    }

            //for (int i = 0; i < 256; i++)
            //    hist[i] = 0;

            //for (int i = 0; i < Height * Width * 4; i += 4)
            //{

            //    hist[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
            //    //int g=(pix[i]+pix[i+1]+pix[i+2])/ 3;
            //    //hist[g]++;
            //    pix[i + 1] = 0;
            //    pix[i + 2] = 0;
            //}

            // --- Histogramm ausgeben
            FillHisto(zedgraph, hist);

            // --- Bild schreiben - hier kopieren wir nur mal

            // --- Writeable Bitmap erzeugen, so gross wie das gelesene Bild
            WriteableBitmap wbm = new WriteableBitmap(cimg);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, Width, Height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, Width * 4, 0);

            image_out.Source = wbm;

        }

        private void ButtonClick_bitSlicing(object sender, RoutedEventArgs e)
        {
            int height = cimg.PixelHeight;
            int width = cimg.PixelWidth;
            byte[] pix = new byte[height * width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, width * 4, 0);
            RadioButton but = (RadioButton)sender;
            int mask = 0x80;
            switch (but.Tag)
            {
                case "7":
                    mask = 0x80;
                    break;
                case "6":
                    mask = 0x40;
                    break;
                case "5":
                    mask = 0x20;
                    break;
                case "4":
                    mask = 0x10;
                    break;
                case "3":
                    mask = 0x08;
                    break;
                case "2":
                    mask = 0x04;
                    break;
                case "1":
                    mask = 0x02;
                    break;
                case "0":
                    mask = 0x01;
                    break;
            }
            for (int i = 0; i < width*height*4; i += 4)
            {
                if (((pix[i + 0] & mask)<< 1) > 0)
                {
                    pix[i] = 255;
                    pix[i + 1] = 255;
                    pix[i + 2] = 255;
                    pix[i + 3] = 255;
                }
                else
                {
                    pix[i] = 0;
                    pix[i + 1] = 0;
                    pix[i + 2] = 0;
                    pix[i + 3] = 255;
                }
            }
                // --- Writeable Bitmap erzeugen, so gross wie das gelesene Bild
                WriteableBitmap wbm = new WriteableBitmap(cimg);

                // --- wir nehmen das ganze Bild
                Int32Rect rect = new Int32Rect(0, 0, width, height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
                wbm.WritePixels(rect, pix, width * 4, 0);
                image_bit_out.Source = wbm;
        }

        //
        // --- Histogramm ausgeben
        //
        private void FillHisto(ZedGraphControl z, int[] hist)
        {         
            // --- Liste der Kurven  löschen -------------
            z.GraphPane.CurveList.Clear();

            // --- Referenz zur GraphPane  (für Text) ----
            GraphPane myPane = z.GraphPane;

            // --- Parameter setzen ----------------------
            myPane.Title.Text = "Histo";
            myPane.XAxis.Title.Text = "Level";
            myPane.YAxis.Title.Text = "N";

            // --- Punktliste füllen ---------------------
            PointPairList listR = new PointPairList();
            listR.Clear();

            for (int i = 0; i < 256; i++)
            {
                listR.Add(i, hist[i]);
            }

            // --- Kurve ins Control ---------------------
            LineItem myCurve_R = myPane.AddCurve("rot", listR, System.Drawing.Color.Red, SymbolType.None);

            // --- Control updaten ----------------------
            z.AxisChange();
            z.Invalidate();

        }

        private void ButtonClick_Bit(object sender, RoutedEventArgs e)
        {

        }
    }
}
