﻿//------------------------------------------------------------------//
// Bildverarbeitung Basics mit WPF                                  //
// 2.4.2017 DS                                                      //
// das zedgraph Control ist in der Toolbox nicht sichtbar, weil     //
// es ein Forms Control ist !                                       //
// hinzunehmen wie in der Aufgabe beschrieben                       //         
//------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Drawing;

// --- ZedGraph -----------------------------
using ZedGraph;


namespace Basics_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage img = new BitmapImage();

        public MainWindow()
        {
            InitializeComponent();

            // --- Bild laden ------------------------

            // --- mit einer Bitmap kann man hier nicht viel anfangen -------
            //Bitmap img1;
            //img1 = new Bitmap(@"..\..\..\Images\trui muster 512.jpg");

            //// --- BitmapImage vom File erzeugen und die Pixel in eine WriteableBitmap schreiben 
            //Stream str = new FileStream(@"..\..\..\Images\lena 512.jpg", FileMode.Open); //trui muster 512.jpg

            //img.BeginInit();
            //img.StreamSource = str;
            //img.EndInit();

            //// --- Grösse in Pixel berechnen ----------------------------  PixelWidth, PixelHeight
            //int width = (int)(img.Width * img.DpiX / 96);
            //int height = (int)(img.Height * img.DpiY / 96);

            //// --- dahin werden die Pixel kopiert -----------------------
            //Array pix = Array.CreateInstance(typeof(Int32), width * height); // 512*512
            //img.CopyPixels(pix, width*4, 0);   //stride: 512 Pixel 4 Byte/Pixel weil Bgra32 !

            //// --- davon BItmapsource erzeugen
            //BitmapSource bs = BitmapSource.Create(width, height, img.DpiX,img.DpiY, PixelFormats.Bgra32, null, pix, width * 4);

            //// --- mit der Source eine schreibbare Bitmap erzeugen -----
            //WriteableBitmap wbm;
            //wbm = new WriteableBitmap(bs);

            // ------------------------------------------------------------ help WriteableBitmap
            // so gehts schneller

            // --- Bild lesen --- File Format beachten !!
            //                    wenns nicht jpg ist, scheint es auch nicht stören !
            //
            BitmapSource srcImage = JpegBitmapDecoder.Create(File.Open(@"..\..\..\Images\Lena 512.jpg", FileMode.Open), BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.OnLoad).Frames[0];

            // --- Format prüfen ! ---------------- wenns nicht Bgra32 ist, dahin konvertieren
            if (srcImage.Format != PixelFormats.Bgra32)
                srcImage = new FormatConvertedBitmap(srcImage, PixelFormats.Bgra32, null, 0);

            WriteableBitmap wbm = new WriteableBitmap(srcImage);

            /* Diese Formate gibt es -------------------------------------
               System.Windows.Media.Imaging.BmpBitmapDecoder
               System.Windows.Media.Imaging.GifBitmapDecoder
               System.Windows.Media.Imaging.IconBitmapDecoder
               System.Windows.Media.Imaging.JpegBitmapDecoder
               System.Windows.Media.Imaging.LateBoundBitmapDecoder
               System.Windows.Media.Imaging.PngBitmapDecoder
               System.Windows.Media.Imaging.TiffBitmapDecoder
               System.Windows.Media.Imaging.WmpBitmapDecoder

             */


            // --- ins Bild schreiben ------------------------------------------------
            int row     = 50;
            int column  = 50;

            // --- Bild locken -------------------------
            wbm.Lock();

            unsafe
            {
                // --- Pointer holen ------------------- 
                uint pBackBuffer = (uint)wbm.BackBuffer;

                // --- Pointer an die gewünschte Stelle setzen 
                pBackBuffer += (uint)(row * wbm.BackBufferStride);
                pBackBuffer += (uint)(column * 4);

                // --- sichern -------------------------
                uint ptemp = pBackBuffer;

                for (int j = 0; j < 20; j++)
                {
                    ptemp = pBackBuffer + (uint)(wbm.BackBufferStride * j);

                    for (int i = 0; i < 120 / 4; i++)
                    {
                        *((uint*)ptemp) = 0xFF00FF00;
                        ptemp += 4;
                    }         
                }
            }

            // --- dieser Bereich hat sich geändert (macht den Update effizienter, geht aber auch ohne )
            wbm.AddDirtyRect(new Int32Rect(column, row, 100, 100));

            // --- Pixelbuffer freigeben -------------------------------
            wbm.Unlock();

            // ----------------------------------------------------------------------

            // --- Bild ans Control binden ------------------------
            image_in.Source = wbm;

            // --- Histogramm erzeugen ----------------------------
            histo(wbm);

            // --- leere WriteableBitmap erzeugen und hineinschreiben 
            // ......

        }

        //
        // --- Histogramm ausgeben 
        //
        public void histo(WriteableBitmap img)
        {
            // --- 256 Werte 32Bit ARGB 
            int[] histR = new int[256];

            // --- Bild festhalten ------------------------------------
            img.Lock();

            unsafe
            {
                // --- Pointer zum Pixelbuffer -------------------- 
                uint pBackBuffer = (uint)img.BackBuffer;

                // --- temp zur Sicherheit ------------------------
                uint p = (uint)pBackBuffer;

                // --- Höhe in Pixel berechnen
                //  img.Height: device unabhängie Einheiten 1/96 inch
                //  img.DpiY  : pixel pro inch
                
                //int height = (int)(img.Height * img.DpiY / 96);
                int height = img.PixelHeight;

                // --- histo füllen -------------------------------
                for (int row = 0; row < height; row++)
                {
                    p = pBackBuffer + (uint)(img.BackBufferStride * row);

                    for (int col = 0; col < img.BackBufferStride/4; col++)
                    {
                        uint pix = *(uint *)p;

                        //pix = (pix & 0x00ff0000)>>16;   //  rot
                        pix = (pix & 0x0000ff00) >> 8;  //  grün
                        //pix = (pix & 0x000000ff) ;      //  blau

                        histR[pix] += 1;

                        p += 4;
                    }
                    
                }

                // --- Bild freigeben ----------------------
                img.Unlock();
            }

            // --- Zed Display füllen  ----------------------------------------

            // --- Liste der Kurven  löschen -------------
            zedgraph.GraphPane.CurveList.Clear();

            // --- Referenz zur GraphPane  (für Text) ----
            GraphPane myPane = zedgraph.GraphPane;

            // --- Parameter setzen ----------------------
            myPane.Title.Text = "Histo";
            myPane.XAxis.Title.Text = "Level";
            myPane.YAxis.Title.Text = "N";

            // --- Punktliste füllen ---------------------
            PointPairList listR = new PointPairList();
            listR.Clear();

            for (int i = 0; i < 256; i++)
            {
                listR.Add(i, histR[i]);
            }

            // --- Kurve ins Control ---------------------
            LineItem myCurve_R = myPane.AddCurve("rot", listR, System.Drawing.Color.Red, SymbolType.None);

            // --- Control updaten ----------------------
            zedgraph.AxisChange();
        }

    }
}
