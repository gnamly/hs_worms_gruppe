﻿//------------------------------------------------------------------//
// Pixel ...                                                        //
//  13.3.2017 DS                                                    //
//   2.4.2017 DS                                                    //
// das Format der Bitmap wird durch die Extension bestimmt          //
// png und jpg enthalten dasselbe Grauwertbild  8Bit/Pixel          //
// png: intern 32 Bit /Pixel                                        //
// jpg:         8 Bit /Pixel                                        //
// #### Formate ! Formate ! aufpassen!!    ########                 //
// GetPixel liefert immer einen 32Bit Colorwert                     //
//------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;

using ZedGraph;

namespace Basics
{
    public partial class Form1 : Form
    {
        Bitmap Img,Img1;
        public Form1()
        {
            InitializeComponent();

            try {
                Img = new Bitmap(@"..\..\..\Images\lena 512.jpg"); // 32bpprgb     32Bit Pixel
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+" : File Open Error ");

                // --- hilft leider nicht viel ! Problem: Zed Control 
                Application.ExitThread();

                Application.Exit();

                return;
            }
            
            pictureBox.Image = Img;

            histo(Img);

            // --- Bild schreiben ---------------------------------------------------------

            // --- neues Bild 512 x 512 -------------------------------
            Img1 = new Bitmap(512,512,PixelFormat.Format32bppArgb);

            // --- Pointer holen , lock -------------------------------
            BitmapData bmpData = Img1.LockBits(new Rectangle(0, 0, Img1.Width, Img1.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb); //PixelFormat.Format32bppArgb

            unsafe
            {
                byte* p = (byte*)bmpData.Scan0;

                for (int j = 0; j < bmpData.Height; j++)
                {
                    // --- Zeile schreiben ----------------------------
                    for (int i = 0; i < bmpData.Width/2; i++)
                    {

                        *(p + 0) = (byte)i;
                        *(p + 1) = (byte)i;
                        *(p + 2) = (byte)i;
                        *(p + 3) = (byte)0xFF;

                        p += 4;

                        *(p + 0) = (byte)i;
                        *(p + 1) = (byte)i;
                        *(p + 2) = (byte)i;
                        *(p + 3) = (byte)0xFF;

                        p += 4;
                    }

                }
            }

            // --- Bild freigeben --------------------------
            Img1.UnlockBits(bmpData);

            // --- anzeigen --------------------------------
            pictureBox1.Image = Img1;

            // --- speichern -------------------------------
            //Img1.Save(@"G:\Stripes.bmp", ImageFormat.Bmp);
        }

        //
        // --- Histogramm
        //
        void histo(Bitmap Img)
        {
            int mode = 1;

            // --- Histogramm Array ------------------------------  
            int[] histo = new int[256];
 
            if(mode == 0) { 
            // --- Histogramm mit GetPixel ----------------------- farbbild: roter Kanal
            for (int i=0; i< Img.Width; i++)
                for(int j=0; j< Img.Height; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    histo[c.R] += 1;
                }

            }
            else { 
            // --- Histogramm über Pointer ----------------------
            BitmapData bmpData = Img.LockBits(new Rectangle(0, 0, Img.Width, Img.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, Img.PixelFormat); //PixelFormat.Format32bppArgb

            // --- Pointer auf erst5es Pixel --------------------
            IntPtr ptr = bmpData.Scan0;

            for (int i = 0; i < 256; i++) histo[i] = 0;

            // --- Format beachten !!
            // byte* liefert bei einem 32 Bit/Pix 4 mal soviele Daten und FF für A !
            //
            unsafe
            {
                byte* p = (byte*)bmpData.Scan0;
  
                // --- loop über alle Pixel    --- und Bytes, wenns ein Farbbild ist !  das ist dann eine Überlagerung der Histogramme für R,G,B !             
                for (int i = 0; i < bmpData.Stride * Img.Height; i++)           //bmpData.Stride ist die Länge der Scanline in Byte
                {
                    byte index = *p;             
                    histo[index]++;
                    p++;
                }

                histo[255] = 0;   // Alpha Kanal !!  löschen sonst sieht man nicht viel

            }

            // --- Bild freigeben ------------------------------
            Img.UnlockBits(bmpData);

            }
            // ---ZedGraph Control füllen ------------------------------------------------------

            // --- Liste löschen -------------------------
            zedGraph_histo.GraphPane.CurveList.Clear();

            // --- Referenz zur  GraphPane
            GraphPane myPane = zedGraph_histo.GraphPane;

            // --- Parameter setzen ----------------------
            myPane.Title.Text       = "Histo";
            myPane.XAxis.Title.Text = "Level";
            myPane.YAxis.Title.Text = "N";

            PointPairList listR = new PointPairList();
            listR.Clear();

            for (int i = 0; i < 256; i++) {
                listR.Add(i, histo[i]); 
            }

            LineItem myCurve_R = myPane.AddCurve("rot", listR, Color.Red, SymbolType.None);

            // --- Control updaten ---------------
            zedGraph_histo.AxisChange();

        }
    }
}
