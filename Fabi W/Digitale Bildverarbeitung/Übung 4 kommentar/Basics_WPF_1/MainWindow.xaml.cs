﻿//------------------------------------------------------------------//
// Bildverarbeitung Basics mit WPF                                  //
// 26.3.2018 DS                                                     //
// 10.4.2018 DS Update save, format convert                         //
//------------------------------------------------------------------//
// das zedgraph Control ist in der Toolbox nicht sichtbar, weil     //
// es ein Forms Control ist !                                       //
// hinzunehmen wie in der Aufgabe beschrieben                       //
// Pixel lesen: alle Pixel in ein Arry kopieren : CopyPixel         //
// Pixel schreiben : das Array wieder ins Bild kopieren : WritePixel//
// Alternative: Pointer auf den BackBuffer holen und in diesem      //
// herumpointern !! Vorsicht !!!                                    //
//------------------------------------------------------------------//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;

using ZedGraph;
using System.IO;                    // zum File lesen/schreiben      
using Microsoft.Win32;              // hier sind die File Dialoge

namespace Basics_WPF_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage img = new BitmapImage();                        // das Bild, damit es von überall zugänglich ist
        FormatConvertedBitmap cimg = new FormatConvertedBitmap();   // das nach Bgr32 konvertierte Bild
        WriteableBitmap wbm = null;                                 // das Bild für das rechte image control

        public MainWindow()
        {
            InitializeComponent();
            
        }

        //
        // --- Bild speichern
        //
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            // wie ?   --- das Netz wusste alles ......

            // --- Save Dialog -------------
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "Bilder |*.jpg; *.bmp; *.png, .tif, .gif, .wmp";
            dlg.ShowDialog();

            if (dlg.FileName == "")
                return;

            // --- je nach Typ einen Encoder erzeugen
            BitmapEncoder enc = null;

            switch (dlg.FileName.ToLower().Split('.').Last())
            {
                case "png":
                    enc = new PngBitmapEncoder();
                    break;

                case "jpg":
                    enc = new JpegBitmapEncoder();                   
                    break;

                case "bmp":
                    enc = new BmpBitmapEncoder();
                    break;

                case "tif":
                    enc = new TiffBitmapEncoder();
                    break;

                case "gif":
                    enc = new GifBitmapEncoder();
                    break;

                case "wmp":
                    enc = new WmpBitmapEncoder();
                    break;
                default:
                    MessageBox.Show("Format nicht unterstützt");
                    return;             // --- kein unterstütztes Format gefunden
            }
  
            // --- das Bild zum Encoder hinzufügen    --- irgendwie sollte festgelegt werden, welches Bild gespeichert werden soll ....
            enc.Frames.Add(BitmapFrame.Create(wbm));

            // --- schreiben 
            using (Stream sw = new FileStream(dlg.FileName, FileMode.Create))
            {
                enc.Save(sw);
            }

        }

        //
        // --- neues Bild laden -- Histogramm berechnen -- sonst was machen
        //
        
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            // --- Dialog erzeugen 
            OpenFileDialog dlg = new OpenFileDialog();

            // --- das richtige Directory wählen -- krampfig! geht das einfacher ?
            string str = Directory.GetCurrentDirectory();       // hier sind wir
            string[] strtk = str.Split('\\');                   // den Pfad aufspalten

            string dir = "";                                    // neu zusammensetzen aber 3 Ebenene höher gehen
            for (int i = 0; i < strtk.Length - 3; i++)
                dir = dir + strtk[i] + "\\";

            dir += "Images\\";                                  // hier ist Images

            // --- sicherheitshalber prüfen, ob es das Verzeichnis gibt
            if (Directory.Exists(dir))
                dlg.InitialDirectory = dir;
            else
                dlg.InitialDirectory = str;

            // --- Dialog anzeigen
            dlg.Filter = "Bilder |*.jpg; *.bmp; *.png";
            dlg.ShowDialog();

            // --- Bild laden und anzeigen
            BitmapImage tmpimg = new BitmapImage();
            tmpimg.BeginInit();
            tmpimg.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            tmpimg.UriSource = new Uri(dlg.FileName, UriKind.RelativeOrAbsolute);
            tmpimg.EndInit();

            image_in.BeginInit();
            image_in.Source = tmpimg;
            image_in.EndInit();


            // --- das Bild wird nach Bgr32 konvertiert - in jedem Fall        !! hier hätte ich gerne cimg gehabt, aber er will das nicht, BeginInit() ist das Problem
            FormatConvertedBitmap cimgtmp = new FormatConvertedBitmap();
            cimgtmp.BeginInit();
            cimgtmp.Source = tmpimg;
            cimgtmp.DestinationFormat = PixelFormats.Bgr32;
            cimgtmp.EndInit();
            cimg = cimgtmp; //für die ganze klasse zur verfügung stellen
            // --- Bilddaten ----------------
            //int Height = (int)(cimgtmp.Height * cimgtmp.DpiY / 96);      // Höhe holen und auf Pixel umrechnen
            //int Width = (int)(cimgtmp.Width * cimgtmp.DpiX / 96);        // Höhe holen und auf Pixel umrechnen
            int Height = cimgtmp.PixelHeight;
            int Width  = cimgtmp.PixelWidth;

            // --- Histogramm berechnen -----

            // --- Pixel aus dem Bild holen - in ein Bytearray passender Grösse speichern
            byte[] pix = new byte[Height * Width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 

            cimgtmp.CopyPixels(pix, Width * 4, 0);

            // --- Histogramm berechnen
            int[] hist = new int[256];

            // --- über alle Pixel weg
            for (int i = 0; i < Height * Width * 4; i += 4)
            {
                hist[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
            }

            // --- Histogramm ausgeben
            FillHisto(zedgraph, hist);
            zedgraph.Invalidate();
            zedgraph.Update();

            greyImage(pix);
            negativImage(pix);
            image_schwell_in.Source = cimg;
            image_trans_in.Source = cimg;
            hsi_in.Source = cimg;
            schwellenImage();
            transformImage();
            HsiOperation();
        }

        private void greyImage(byte[] pix)
        {
            int Width = cimg.PixelWidth;
            int Height = cimg.PixelHeight;
            for (int i = 0; i < Height * Width * 4; i += 4)
            {
                int grey = (pix[i] + pix[i + 1] + pix[i+2]) / 3;
                pix[i] = (byte)grey;
                pix[i + 1] = (byte)grey;
                pix[i + 2] = (byte)grey;

            }

            // --- das geladene Bild ins rechte image control schreiben
            // --- Writeable Bitmap erzeugen, so gross wie das gelesene convertierte Bild
            wbm = new WriteableBitmap(cimg);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, Width, Height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, Width * 4, 0);

            image_out.Source = wbm;
        }

        private void negativImage(byte[] pix)
        {
            int Width = cimg.PixelWidth;
            int Height = cimg.PixelHeight;
            for (int i = 0; i < Height * Width * 4; i += 4)
            {
                pix[i] = (byte)(1-pix[i]);
                pix[i + 1] = (byte)(1 - pix[i+1]);
                pix[i + 2] = (byte)(1 - pix[i+2]);
            }

            // --- das geladene Bild ins rechte image control schreiben
            // --- Writeable Bitmap erzeugen, so gross wie das gelesene convertierte Bild
            wbm = new WriteableBitmap(cimg);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, Width, Height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, Width * 4, 0);

            image_neg_out.Source = wbm;
        }

        private void schwellenImage()
        {
            int schwelle = (int)schwellen_slider.Value;

            int height = cimg.PixelHeight;
            int width = cimg.PixelWidth;
            byte[] pix = new byte[height * width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, width * 4, 0);

            for (int i = 0; i < height * width * 4; i += 4)
            {
                if (pix[i] <= schwelle)
                {
                    pix[i] = 0;
                    pix[i + 1] = 0;
                    pix[i + 2] = 0;
                }
                else
                {
                    pix[i] = 255;
                    pix[i + 1] = 255;
                    pix[i + 2] = 255;
                }
            }
            ZedGraphControl z = schwellengraph;
            // --- Liste der Kurven  löschen -------------
            z.GraphPane.CurveList.Clear();
            z.GraphPane.YAxis.Scale.Min = 0;
            z.GraphPane.YAxis.Scale.Max = 270;
            z.GraphPane.XAxis.Scale.Max = 255;
            // --- Referenz zur GraphPane  (für Text) ----
            GraphPane myPane = z.GraphPane;

            // --- Parameter setzen ----------------------
            myPane.Title.Text = "Histo";
            myPane.XAxis.Title.Text = "Level";
            myPane.YAxis.Title.Text = "N";

            // --- Punktliste füllen ---------------------
            PointPairList listR = new PointPairList();
            listR.Clear();

            for (int i = 0; i <= schwelle; i++)
            {
                listR.Add(i, 0);
            }
            for (int i = schwelle+1; i <= 255; i++)
            {
                listR.Add(i, 255);
            }

            // --- Kurve ins Control ---------------------
            LineItem myCurve_R = myPane.AddCurve("rot", listR, System.Drawing.Color.Red, SymbolType.None);

            // --- Control updaten ----------------------
            z.AxisChange();
            z.Invalidate();

            // --- das geladene Bild ins rechte image control schreiben
            // --- Writeable Bitmap erzeugen, so gross wie das gelesene convertierte Bild
            wbm = new WriteableBitmap(cimg);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, width, height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, width * 4, 0);

            image_schwell_out.Source = wbm;
        }

        private void transformImage()
        {
            byte[] pix = new byte[cimg.PixelHeight * cimg.PixelWidth * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, cimg.PixelWidth * 4, 0);

            //histogram berechnen
            int[] hist = new int[256];
            // --- über alle Pixel weg
            for (int i = 0; i < cimg.PixelHeight * cimg.PixelWidth * 4; i += 4)
            {
                hist[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
            }

            //lut anlegen
            int[] lut = new int[256];
            double sum = 0.0;
            int max = (int)(255.0 * trans_slider.Value);
            int n = cimg.PixelHeight * cimg.PixelWidth;
            for (int i = 0; i < 256; i++)
            {
                sum += hist[i];
                lut[i] = (int)(sum * max / n);
            }

            //output with lut to pix array
            for (int i = 0; i < n * 4; i += 4)
            {
                pix[i] = (byte)lut[pix[i]];
                pix[i + 1] = (byte)lut[pix[i + 1]];
                pix[i + 2] = (byte)lut[pix[i + 2]];
            }

            //histogram berechnen
            int[] histLut = new int[256];
            // --- über alle Pixel weg
            for (int i = 0; i < cimg.PixelHeight * cimg.PixelWidth * 4; i += 4)
            {
                histLut[pix[i]]++;                         //i: B  i+1: G i+2: R i+3: A
            }

            //output histo & transhisto
            FillHisto(transHisto_orginal, hist);
            FillHisto(transHisto_trans, histLut);
            zedgraph.Invalidate();
            zedgraph.Update();

            //output array to image
            wbm = new WriteableBitmap(cimg);

            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, cimg.PixelHeight, cimg.PixelWidth);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pix, cimg.PixelWidth * 4, 0);

            image_trans_out.Source = wbm;
        }

        private void HsiOperation()
        {
            byte[] pix = new byte[cimg.PixelHeight * cimg.PixelWidth * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, cimg.PixelWidth * 4, 0);

            int n = cimg.PixelHeight * cimg.PixelWidth;

            int[] histH = new int[256];
            int[] histS = new int[256];
            int[] histI = new int[256];

            byte[] pixH = new byte[n * 4];
            byte[] pixS = new byte[n * 4];
            byte[] pixI = new byte[n * 4];
      

            float[] hsiPix = RGBtoHSI(cimg); //+++  auf 4 Byte / Pixel gesetzt

            for (int i = 0; i < hsiPix.Length; i += 4)  //+++ über 4 Byte laufen
            {
                int hi = (int)((hsiPix[i] / 360.0) * 255);
                int si = (int)(hsiPix[i + 1] * 255);
                int bi = (int)(hsiPix[i + 2] * 255);

                //+++  die einzelnen Kanalwerte als Grauwerte in die Kanalbilder schreiben
                pixI[i] = (byte)(bi/3);
                pixI[i + 1] = (byte)(bi / 3);
                pixI[i + 2] = (byte)(bi / 3);
                pixI[i + 3] = 255;

                pixH[i] = (byte)(hi / 3);
                pixH[i + 1] = (byte)(hi / 3);
                pixH[i + 2] = (byte)(hi / 3);
                pixH[i + 3] = 255;

                pixS[i] = (byte)(si / 3);
                pixS[i + 1] = (byte)(si / 3);
                pixS[i + 2] = (byte)(si / 3);
                pixS[i + 3] = 255;
                //+++

                histH[hi]++;
                histS[si]++;
                histI[bi]++;
            }

            //LUT  +++ wozu ?
            int[] lutH = new int[256];
            int[] lutS = new int[256];
            int[] lutI = new int[256];
            double sumH = 0.0;
            double sumS = 0.0;
            double sumI = 0.0;
            int max = (int)(255.0 * hsi_slider.Value);
            //int n = cimg.PixelHeight * cimg.PixelWidth;
            for (int i = 0; i < 256; i++)
            {
                sumH += histH[i];
                sumH += histS[i];
                sumH += histI[i];
                lutH[i] = (int)(sumH * max / n);
                lutS[i] = (int)(sumS * max / n);
                lutI[i] = (int)(sumI * max / n);
            }

            //byte[] pixH = new byte[n * 4];
            //byte[] pixS = new byte[n * 4];
            //byte[] pixI = new byte[n * 4];
            ////output with lut
            //for (int i = 0; i < n * 4; i += 4)
            //{
            //    pixH[i] = (byte)lutH[pixH[i]];
            //    pixH[i + 1] = (byte)lutH[pixH[i + 1]];
            //    pixH[i + 2] = (byte)lutH[pixH[i + 2]];
            //    pixH[i + 3] = 255; //Setting A channel 
            //    pixS[i] = (byte)lutS[pixS[i]];
            //    pixS[i + 1] = (byte)lutS[pixS[i + 1]];
            //    pixS[i + 2] = (byte)lutS[pixS[i + 2]];
            //    pixS[i + 3] = 255; //Setting A channel 
            //    pixI[i] = (byte)lutI[pixI[i]];
            //    pixI[i + 1] = (byte)lutI[pixI[i + 1]];
            //    pixI[i + 2] = (byte)lutI[pixI[i + 2]];
            //    pixI[i + 3] = 255; //Setting A channel 
            //}

            //for (int i = 0;i < n * 4; i += 4)
            //{
            //    int r;
            //    int g;
            //    int b;
            //    HlsToRgb(pixH[i], pixH[i + 1], pixH[i + 2], out r, out g, out b);
            //    pix[i] = (byte)b;
            //    pix[i + 1] = (byte)g;
            //    pix[i + 2] = (byte)r;
            //    HlsToRgb(pixS[i], pixS[i + 1], pixS[i + 2], out r, out g, out b);
            //    pix[i] = (byte)b;
            //    pix[i + 1] = (byte)g;
            //    pix[i + 2] = (byte)r;
            //    HlsToRgb(pixI[i], pixI[i + 1], pixI[i + 2], out r, out g, out b);
            //    pix[i] = (byte)b;
            //    pix[i + 1] = (byte)g;
            //    pix[i + 2] = (byte)r;
            //}

            wbm = new WriteableBitmap(cimg);
            // --- wir nehmen das ganze Bild
            Int32Rect rect = new Int32Rect(0, 0, cimg.PixelWidth, cimg.PixelHeight);
            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pixH, cimg.PixelWidth * 4, 0);
            hsi_out_h.Source = wbm;
       
            wbm = new WriteableBitmap(cimg);
            // --- wir nehmen das ganze Bild
            rect = new Int32Rect(0, 0, cimg.PixelWidth, cimg.PixelHeight);
            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pixS, cimg.PixelWidth * 4, 0);
            hsi_out_s.Source = wbm;

            wbm = new WriteableBitmap(cimg);
            // --- wir nehmen das ganze Bild
            rect = new Int32Rect(0, 0, cimg.PixelWidth, cimg.PixelHeight);
            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
            wbm.WritePixels(rect, pixI, cimg.PixelWidth * 4, 0);
            hsi_out_i.Source = wbm;
        }

        /// <summary>
        /// Returns a array (width * height * 3) i=h i+1=s i+2=i  h(0-360) s(0-1) i(0-1)  
        /// </summary>
        /// <param name="cimgrgb"></param>
        /// <returns></returns>
        private float[] RGBtoHSI(FormatConvertedBitmap cimgrgb)
        {
            FormatConvertedBitmap cimgtmp = cimgrgb;
            byte[] pix = new byte[cimgtmp.PixelHeight * cimgtmp.PixelWidth * 4];   //+++ 4           // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, cimgtmp.PixelWidth * 4, 0);
            float[] hsiPix = new float[cimgtmp.PixelHeight * cimgtmp.PixelWidth * 4]; //+++ 4   3Byte scheinen mir zu umstaendlich, weil man immer daran denken muss
            int j = 0;
            for (int i = 0;i < cimgtmp.PixelHeight * cimgtmp.PixelWidth * 4; i+=4)
            {
                System.Drawing.Color orginal = System.Drawing.Color.FromArgb(pix[i + 2], pix[i + 1], pix[i]);
                float h = orginal.GetHue();               //0 - 360
                float s = orginal.GetSaturation(); //0 - 1
                float b = orginal.GetBrightness();  //0 - 1
                hsiPix[j] = h;
                hsiPix[j+1] = s;
                hsiPix[j+2] = b;
                j += 4; //+++
            }
            return hsiPix;
        }
        public static void RgbToHls(int r, int g, int b,
    out double h, out double l, out double s)
        {
            // Convert RGB to a 0.0 to 1.0 range.
            double double_r = r / 255.0;
            double double_g = g / 255.0;
            double double_b = b / 255.0;

            // Get the maximum and minimum RGB components.
            double max = double_r;
            if (max < double_g) max = double_g;
            if (max < double_b) max = double_b;

            double min = double_r;
            if (min > double_g) min = double_g;
            if (min > double_b) min = double_b;

            double diff = max - min;
            l = (max + min) / 2;
            if (Math.Abs(diff) < 0.00001)
            {
                s = 0;
                h = 0;  // H is really undefined.
            }
            else
            {
                if (l <= 0.5) s = diff / (max + min);
                else s = diff / (2 - max - min);

                double r_dist = (max - double_r) / diff;
                double g_dist = (max - double_g) / diff;
                double b_dist = (max - double_b) / diff;

                if (double_r == max) h = b_dist - g_dist;
                else if (double_g == max) h = 2 + r_dist - b_dist;
                else h = 4 + g_dist - r_dist;

                h = h * 60;
                if (h < 0) h += 360;
            }
        }
        public static void HlsToRgb(double h, double l, double s,
    out int r, out int g, out int b)
        {
            double p2;
            if (l <= 0.5) p2 = l * (1 + s);
            else p2 = l + s - l * s;

            double p1 = 2 * l - p2;
            double double_r, double_g, double_b;
            if (s == 0)
            {
                double_r = l;
                double_g = l;
                double_b = l;
            }
            else
            {
                double_r = QqhToRgb(p1, p2, h + 120);
                double_g = QqhToRgb(p1, p2, h);
                double_b = QqhToRgb(p1, p2, h - 120);
            }

            // Convert RGB to the 0 to 255 range.
            r = (int)(double_r * 255.0);
            g = (int)(double_g * 255.0);
            b = (int)(double_b * 255.0);
        }
        private static double QqhToRgb(double q1, double q2, double hue)
        {
            if (hue > 360) hue -= 360;
            else if (hue < 0) hue += 360;

            if (hue < 60) return q1 + (q2 - q1) * hue / 60;
            if (hue < 180) return q2;
            if (hue < 240) return q1 + (q2 - q1) * (240 - hue) / 60;
            return q1;
        }
        //
        // --- exit -  nach save fragen !
        //
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //  --- save ? Message Box
            MessageBoxResult res =  MessageBox.Show(" möchten sie speichern ? ", "save", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (res != MessageBoxResult.No)
                Save_Click(null, new RoutedEventArgs());
                 
            Application.Current.Shutdown();
           
        }

        // 
        // --- wird aufgerufen, wenn das Window geladen ist, dann ist alles initialisiert
        //
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Load_Click(new object(), new RoutedEventArgs());
        }

        private void ButtonClick_bitSlicing(object sender, RoutedEventArgs e)
        {
            int height = cimg.PixelHeight;
            int width = cimg.PixelWidth;
            byte[] pix = new byte[height * width * 4];              // Stride ist Width*4 weil angenommen wird, das Pixelformat ist 4 Byte 
            cimg.CopyPixels(pix, width * 4, 0);
            RadioButton but = (RadioButton)sender;
            int mask = 0x80;
            switch (but.Tag)
            {
                case "7":
                    mask = 0x80;
                    break;
                case "6":
                    mask = 0x40;
                    break;
                case "5":
                    mask = 0x20;
                    break;
                case "4":
                    mask = 0x10;
                    break;
                case "3":
                    mask = 0x08;
                    break;
                case "2":
                    mask = 0x04;
                    break;
                case "1":
                    mask = 0x02;
                    break;
                case "0":
                    mask = 0x01;
                    break;
            }
            for (int i = 0; i < width*height*4; i += 4)
            {
                if (((pix[i + 0] & mask)<< 1) > 0)
                {
                    pix[i] = 255;
                    pix[i + 1] = 255;
                    pix[i + 2] = 255;
                    pix[i + 3] = 255;
                }
                else
                {
                    pix[i] = 0;
                    pix[i + 1] = 0;
                    pix[i + 2] = 0;
                    pix[i + 3] = 255;
                }
            }
                // --- Writeable Bitmap erzeugen, so gross wie das gelesene Bild
                WriteableBitmap wbm = new WriteableBitmap(cimg);

                // --- wir nehmen das ganze Bild
                Int32Rect rect = new Int32Rect(0, 0, width, height);

            // --- hineinschreiben -- die Pixel, die wir oben aus dem Eingabebild gelesen haben
            //     ändern sie mal das pix Array !
                wbm.WritePixels(rect, pix, width * 4, 0);
                image_bit_out.Source = wbm;
        }

        //
        // --- Histogramm ausgeben
        //
        private void FillHisto(ZedGraphControl z, int[] hist)
        {         
            // --- Liste der Kurven  löschen -------------
            z.GraphPane.CurveList.Clear();

            // --- Referenz zur GraphPane  (für Text) ----
            GraphPane myPane = z.GraphPane;

            // --- Parameter setzen ----------------------
            myPane.Title.Text = "Histo";
            myPane.XAxis.Title.Text = "Level";
            myPane.YAxis.Title.Text = "N";

            // --- Punktliste füllen ---------------------
            PointPairList listR = new PointPairList();
            listR.Clear();

            for (int i = 0; i < 256; i++)
            {
                listR.Add(i, hist[i]);
            }

            // --- Kurve ins Control ---------------------
            LineItem myCurve_R = myPane.AddCurve("rot", listR, System.Drawing.Color.Red, SymbolType.None);

            // --- Control updaten ----------------------
            z.AxisChange();
            z.Invalidate();

        }

        private void schwellen_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            schwellenImage();
        }

        private void trans_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            transformImage();
        }

        private void hsi_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            HsiOperation();
        }
    }
}
