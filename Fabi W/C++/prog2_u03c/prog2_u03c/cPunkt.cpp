#include "cPunkt.h"

cPunkt::cPunkt(float a = 0, float b = 0)
{
	if (a < -10)
		a = -10;
	if (a > 10)
		a = 10;
	if (b < -10)
		b = -10;
	if (b > 10)
		b = 10;
	x = a;
	y = b;
}


cPunkt::~cPunkt()
{
}

void cPunkt::ausgabe()
{
	cout << "(" << x << "|" << y << ")";
}

float cPunkt::getX()
{
	return x;
}

float cPunkt::getY()
{
	return y;
}