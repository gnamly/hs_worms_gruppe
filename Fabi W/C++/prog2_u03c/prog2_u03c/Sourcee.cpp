#include "cDreieck.h"
#include "cPunkt.h"
using namespace std;

void main()
{
	cPunkt p1 = cPunkt(14.3, 2.41);
	cPunkt p2 = cPunkt(1.24, -16.3);
	cPunkt p3 = cPunkt(-4.73, 6.42);
	cDreieck dr1 = cDreieck(p1,p2,p3);
	cDreieck dr2 = cDreieck(14.3,2.41,1.24,-16.3,-4.73,6.42);
	dr1.ausgabe();
	dr2.ausgabe();
}