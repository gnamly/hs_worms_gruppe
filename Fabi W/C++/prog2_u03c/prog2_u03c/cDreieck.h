#pragma once
#include "cPunkt.h"
#include <math.h>
#include <iostream>
using namespace std;
class cDreieck
{
private:
	cPunkt p1;
	cPunkt p2;
	cPunkt p3;

	float umfangD();
	float inhaltD();
public:
	cDreieck(float = 0, float = 0, float = 0, float = 0, float = 0, float = 0);
	cDreieck(cPunkt = cPunkt(0.0, 1.0), cPunkt = cPunkt(1.0, 0.0), cPunkt = cPunkt(0.0, 0.0));
	~cDreieck();
	void ausgabe();
};