#pragma once
#include <iostream>
using namespace std;
class cPunkt
{
private:
	float x;
	float y;
public:
	cPunkt(float, float);
	~cPunkt();
	void ausgabe();
	float getX();
	float getY();
};

