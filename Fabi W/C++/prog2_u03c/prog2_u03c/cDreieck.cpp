#include "cDreieck.h"

float cDreieck::umfangD()
{
	return sqrt(pow(p2.getX() - p1.getX(), 2) + pow(p2.getY() - p1.getY(), 2)) + sqrt(pow(p3.getY() - p2.getX(), 2) + pow(p3.getY() - p2.getX(), 2)) + sqrt(pow(p1.getY() - p2.getX(), 2) + pow(p1.getY() - p3.getX(), 2));
}

float cDreieck::inhaltD()
{
	float a = sqrt(pow(p2.getX() - p1.getX(), 2) + pow(p2.getY() - p1.getY(), 2));
	float b = sqrt(pow(p3.getX() - p2.getX(), 2) + pow(p3.getY() - p2.getY(), 2));
	float c = sqrt(pow(p1.getX() - p3.getX(), 2) + pow(p1.getY() - p3.getY(), 2));
	float s = umfangD() / 2;
	float wu = s*(s - a)*(s - b)*(s - c);
	if (wu < 0)
		wu *= -1;
	float erg = sqrt(wu);
	return erg;
}

cDreieck::cDreieck(float x1, float y1, float x2, float y2, float x3, float y3) : p1(cPunkt(x1,y1)), p2(cPunkt(x2,y2)), p3(x3,y3)
{
}

cDreieck::cDreieck(cPunkt punkt1, cPunkt punkt2, cPunkt punkt3) : p1(punkt1), p2(punkt2), p3(punkt3)
{
}

cDreieck::~cDreieck()
{
}

void cDreieck::ausgabe()
{
	cout << "Dreieck: ";
	p1.ausgabe();
	cout << ",";
	p2.ausgabe();
	cout << ",";
	p3.ausgabe();
	cout << endl;
	cout << "Umfang: " << umfangD() << endl;
	cout << "Flaecheninhalt: " << inhaltD() << endl;
}
