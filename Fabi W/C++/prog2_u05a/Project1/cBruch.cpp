#include"cBruch.h"

int cBruch::ggT()
{
	int a = numerator;
	int b = denominator;

	if (a < 0)
	{
		a *= -1;
	}

	while (b)
	{
		if (a>b) 
		{ 
			a += b; b = a - b; a -= b; 
		}
		b -= a;
	}
	return a;
}

cBruch::cBruch()
{
	numerator = 1;
	denominator = 1;
}

cBruch::cBruch(int numerator, int denominator) : numerator(numerator), denominator(denominator)
{
	if (cBruch::denominator == 0)
		cBruch::denominator = 1;
	if (cBruch::denominator < 0)
	{
		cBruch::denominator = cBruch::denominator*-1;
		cBruch::numerator = cBruch::numerator*-1;
	}

	reduce();
}

cBruch::~cBruch()
{

}

void cBruch::output()
{
	float erg = (float)numerator / (float)denominator;
	cout << numerator << "/" << denominator << " == " << erg << endl;
}

void cBruch::reduce()
{
	int ggt = ggT();
	numerator = numerator / ggt;
	denominator = denominator / ggt;
}