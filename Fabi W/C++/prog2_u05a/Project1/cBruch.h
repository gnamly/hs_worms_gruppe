#pragma once
#include <iostream>

using namespace std;

class cBruch
{
	friend cBruch add(cBruch a, cBruch b);
	friend cBruch sub(cBruch a, cBruch b);
	friend cBruch mul(cBruch a, cBruch b);
	friend cBruch div(cBruch a, cBruch b);
private:
	int numerator;
	int denominator;
	int ggT();

public:
	cBruch();
	cBruch(int, int);
	~cBruch();
	void output();
	void reduce();
};