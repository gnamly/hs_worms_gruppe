#include"cBruch.h"

cBruch add(cBruch a, cBruch b)
{
	cBruch erg(1, 1);
	erg.numerator = a.numerator * b.denominator + a.denominator * b.numerator;
	erg.denominator = a.denominator * b.denominator;
	erg.reduce();

	return erg;
}

cBruch sub(cBruch a, cBruch b)
{
	cBruch erg(1, 1);
	erg.numerator = a.numerator * b.denominator - a.denominator * b.numerator;
	erg.denominator = a.denominator * b.denominator;
	erg.reduce();

	return erg;
}

cBruch mul(cBruch a, cBruch b)
{
	cBruch erg(1, 1);
	erg.numerator = a.numerator * b.numerator;
	erg.denominator = a.denominator * b.denominator;
	erg.reduce();

	return erg;
}

cBruch div(cBruch a, cBruch b)
{
	cBruch erg(1, 1);
	erg.numerator = a.numerator * b.denominator;
	erg.denominator = a.denominator * b.numerator;
	erg.reduce();

	return erg;
}

void main()
{
	cBruch cBrArr[8] = {cBruch(3, 4), cBruch(25, -5), cBruch(-5, -3), cBruch(-14, 22), cBruch(21, 45), cBruch(7, -9), cBruch(2, 3)};

	for (int i = 0; i < 8; i++)
	{
		cBrArr[i].output();
	}
}