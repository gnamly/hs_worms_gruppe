//Übungsaufgabe u03a
//Fabian Weinhold

#include <iostream>
#include <string>
#include "cWindrad.cpp"

using namespace std;

int getMaxLength(string sortingList[1000])
{
	int length = 11;
	for (int i = 0; i < 1000; i++)
	{
		if (sortingList[i].length() > length)
			length = sortingList[i].length();
	}
	return length;
}

void main() {
	cWindrad wr1("Windstorm13", 131.8, 320, 48.3, 8.72),
		wr2("Watt4Watt78", 192.7, 730, 56.76, 5.80),
		wr3;
	wr1.ausgabe();
	wr2.ausgabe();
	wr3.ausgabe();

	cWindrad wr[1000];

	//create
	for (int i = 0; i < 1000; i++)
	{
		wr[i] = cWindrad();
		wr[i].eingabe();
		if (wr[i].getType() == "-")
			break;
	}

	string types[1000];
	string heights[1000];
	string powers[1000];
	string lats[1000];
	string lons[1000];
	//read
	for (int i = 0; i < 1000; i++)
	{
		types[i] = wr[i].getType();
		heights[i] = to_string(wr[i].getHeight());
		powers[i] = to_string(wr[i].getPower());
		lats[i] = to_string(wr[i].getLat());
		lons[i] = to_string(wr[i].getLon());
	}
	int typeLen = getMaxLength(types);
	int heightLen = getMaxLength(heights);
	int powerLen = getMaxLength(powers);
	int latLen = getMaxLength(lats);
	int lonLen = getMaxLength(lons);
	string outputs[1000];
	string typeHeader = "type       ";
	string heightHeader = "Hoehe      ";
	string powerHeader = "Leistung   ";
	string latHeader = "Breitengrad";
	string lonHeader = "Laengengrad";
	for (int i = 0; i < (typeLen - typeHeader.length());i++) 
	{
		typeHeader += " ";
	}
	for (int i = 0; i < (heightLen - heightHeader.length()); i++)
	{
		heightHeader += " ";
	}
	for (int i = 0; i < (powerLen - powerHeader.length()); i++)
	{
		powerHeader += " ";
	}
	for (int i = 0; i < (latLen - latHeader.length()); i++)
	{
		latHeader += " ";
	}
	for (int i = 0; i < (lonLen - lonHeader.length()); i++)
	{
		lonHeader += " ";
	}
	cout << typeHeader << " | " << heightHeader << " | " << powerHeader << " | " << latHeader << " | " << lonHeader << endl;
	for (int i = 0; i < 1000; i++)
	{
		if (types[i] != "-")
		{
			string output = "";
			output += types[i];
			for (int j = 0; j < (typeLen - types[i].length());j++)
			{
				output += " ";
			}
			output += " | ";
			output += heights[i];
			for (int j = 0; j < (heightLen - heights[i].length());j++)
			{
				output += " ";
			}
			output += " | ";
			output += powers[i];
			for (int j = 0; j < (powerLen - powers[i].length());j++)
			{
				output += " ";
			}
			output += " | ";
			output += lats[i];
			for (int j = 0; j < (latLen - lats[i].length()); j++)
			{
				output += " ";
			}
			output += " | ";
			output += lons[i];
			outputs[i] = output;
		}
	}
	for (int i = 0; i < 1000; i++)
	{
		if (types[i] == "-" || types[i] == "")
			break;
		
		cout << outputs[i] << endl;
	}
}