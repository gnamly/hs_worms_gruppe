#include <string>
#include <iostream>

using namespace std;

class cWindrad
{
private:
	string type;
	float height;
	float power;
	float position[2];
public:

private:
	float korrHoehe(float h, float lat, float lon)
	{
		if (height > 200.0 && lat > 54.5 && lon < 6.8) //Nordsee Offshore
		{
			h = 200;
		}
		else if (height > 120.0)
		{
			h = 120;
		}
		return h;
	}
public:
	cWindrad(string t = "-", float h = 130.0, float pow = 0, float lat = 0, float lon = 0) : type(t), height(h), power(pow)
	{
		position[0] = lat;
		position[1] = lon;

		korrHoehe(height, position[0], position[1]);
	}

	~cWindrad()
	{
	}

	void eingabe()
	{
		cout << endl << "Bitte gegen sie die Art des Windrades an: ";
		cin >> type;
		if (type == "-")
		{
			return;
		}
		cout << endl << "Bitte geben sie die Hoehe an: ";
		cin >> height;
		cout << endl << "Bitte geben sie die Leistung an: ";
		cin >> power;
		cout << endl << "Bitte geben sie den Breitengrad an: ";
		cin >> position[0];
		cout << endl << "Bitte geben sie den Lšngengrad an: ";
		cin >> position[1];
		cout << endl;
		korrHoehe(height, position[0], position[1]);
		return;
	}

	void ausgabe()
	{
		cout << "Type: " << type << "\t| Hoehe: " << height << "\t| Leistung: " << power << "\t| Breitengrad: " << position[0] << "\t| Laengengrad: " << position[1] << endl;
	}

	string getType()
	{
		return type;
	}
	float getHeight()
	{
		return height;
	}
	float getPower()
	{
		return power;
	}
	float getLat()
	{
		return position[0];
	}
	float getLon()
	{
		return position[1];
	}
};