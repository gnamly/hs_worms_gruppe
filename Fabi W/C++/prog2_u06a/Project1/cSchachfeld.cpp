#include "cSchachfeld.h"

cSchachfeld::cSchachfeld()
{

}

cSchachfeld::cSchachfeld(xField x, int y, cSchachfeld * prev_in) : x(x), y(y), prev(prev_in)
{
	if (x == xField::C && y == 1)
	{
		delete this;
	}
}

cSchachfeld * cSchachfeld::newSchachfeld(xField x, int y)
{
	return new cSchachfeld(x, y, this);
}

void cSchachfeld::springerZug()
{
	char x_in;
	int y_in;

	cout << "Bitte geben sie die X Koordinate (A - E) ein: ";
	cin >> x_in;
	cout << "Bitte geben sie die Y Koordinate (1 - 8) ein: ";
	cin >> y_in;
	if (y_in < 1 || y_in > 8)
	{
		cout << "Nicht zul�ssige Y koordinate. Bitte nochmal eingeben..." << endl;
		springerZug();
		return;
	}
	switch (x_in)
	{
		case 'A':
			newSchachfeld(xField::A, y_in);
			break;
		case 'B':
			newSchachfeld(xField::B, y_in);
			break;
		case 'C':
			newSchachfeld(xField::C, y_in);
			break;
		case 'D':
			newSchachfeld(xField::D, y_in);
			break;
		case 'E':
			newSchachfeld(xField::E, y_in);
			break;
		case 'F':
			newSchachfeld(xField::F, y_in);
			break;
		case 'G':
			newSchachfeld(xField::G, y_in);
			break;
		case 'H':
			newSchachfeld(xField::H, y_in);
			break;
		default:
			cout << "Nicht zul�ssige X koordinate. Bitte nochmal eingeben..." << endl;
			springerZug();
			return;
	}
}

cSchachfeld::~cSchachfeld()
{
	char x_out = ' ';
	switch (x)
	{
		case 1:
			x_out = 'A';
			break;
		case 2:
			x_out = 'B';
			break;
		case 3:
			x_out = 'C';
			break;
		case 4:
			x_out = 'D';
			break;
		case 5:
			x_out = 'E';
			break;
		case 6:
			x_out = 'F';
			break;
		case 7:
			x_out = 'G';
			break;
		case 8:
			x_out = 'H';
			break;
	}
	cout << "Destruktor: X: " << x_out << " Y: " << y << endl;
	if(prev != NIL)
		delete prev;
}
