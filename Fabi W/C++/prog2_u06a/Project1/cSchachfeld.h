#pragma once
#include <iostream>

using namespace std;

#define NIL (cSchachfeld *)0

class cSchachfeld
{
public:
	enum xField { A = 1, B, C, D, E, F, G, H };
private:
	xField x;
	int y;
	cSchachfeld * prev;
public:
	cSchachfeld();
	cSchachfeld(xField x, int y, cSchachfeld * prev_in = NIL);
	cSchachfeld * newSchachfeld(xField x, int y);
	void springerZug();
	~cSchachfeld();
};
