/* prog_p04c.c 
 * Beschreibung : Praktikumsaufgabe p04c
 * Name         : Fabian Weinhold 
 * Datum        : 14.11.2017
 */
 
 #include <stdio.h>
 #include <stdbool.h>
 
 bool isPrim(int a, int teiller)
 {
	 if(a == 1)							//Sonderfall 1
	 {
		 return true;
	 }
	 if(a < 2)							//Zahl ist zu klein
	 {
		 return false;
	 }
	 else if(teiller == a)				//Primzahl durch sich selber
	 {
		 return true;
	 }
	 if(a % teiller == 0)				//nicht nur durch sich teilbar
	 {
		 return false;
	 }
	 return isPrim(a, teiller+1);		//Noch nicht Fertig, dann weiter suchen
 }
 
 void doPFZ(int a, int prim)
 {
	 if(a == prim)						//Ende der Zerlegung
	 {
		 printf("%i", prim);
		 return;
	 }
	 if(a % prim == 0)					//Wenn es Zerlegbar ist, den Rest Weiter Zerlegen
	 {
		 printf("%i * ",prim);
		 doPFZ(a/prim, prim);
	 }
	 else								//Wenns es nicht Zerlegbar ist mit der nächsten primzahl versuchen
	 {
		 bool isNextPrim = false;
		 while(!isNextPrim)				//Solange noch nicht gefunden ...
		 {
			 if(isPrim(prim+1, 2))		//naechste Gefunden
			 {
				 prim++;
				 isNextPrim = true;
			 }
			 else						//Noch nicht gefunden
			 {
				 prim++;
			 }
		 }
		 doPFZ(a, prim);				//mit überschriebener variable prim
	 }
 }
 
 int main()
 {
	 int max = 0;					//platzhalter fuer die Maximal eingabe
	 int n = 0;						//platzhalter fuer die Zerlegung
	 
	 printf("Geben sie einen Maximalwert der Primzahlensuche ein: \n");
	 scanf("%i", &max);				//Abfragen der Eingabe
	 
	 for(int i = 0;i<max;i++)		//Alle Zahlen bis max durchgehen
	 {
		if(isPrim(i,2))				//Wenn die abgefragte Zahl eine Primzahl ist ...
		{
			if(isPrim(i-2,2))		//kleiner Prim Nachbar?
			{
				printf("Paar gefunden: (%i;%i)\n", i, i-2);		//Paar ausgeben
			}
			if(isPrim(i+2,2) && i+2 < max)		//großer Prim Nachbar? und an der Obergrenze abschneiden
			{
				printf("Paar gefunden: (%i;%i)\n", i, i+2);		//Paar ausgeben
			}
		}
	 }
	 
	 printf("Geben sie eine Zahl fuer die Primfaktorzerlegung ein:\n");
	 scanf("%i", &n);				//Eingabe der zu zerlegenden Zahl
	 printf("Zerlegung: ");				//Beginn der Zerlegungsausgabe
	 doPFZ(n, 2);						//Zerlegung der eingegebenen Zahl
	 printf("\n");						//Abschließendr Zeilenumbruch
	 
	return 0;
 }