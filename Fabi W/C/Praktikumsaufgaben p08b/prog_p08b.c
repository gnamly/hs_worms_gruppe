/* prog_p08b.c
 * Beschreibung : Praktikumsaufgabe p08b
 * Name         : Fabian Weinhold 
 * Datum        : 19.12.2017
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

struct teilnehmer
{
	char vorname[30];
	char nachname[30];
	char beruf[30];
	int gesprarche;
	int tischnummern;
};

int main()
{
	struct teilnehmer member[6] = {{"Heinz", "Hose", "Schneidermeister", 1, 1},
		{"Rudi", "Rohrfrei", "Wasserinstallatuer", 1, 1},
		{"Petra", "Planlos", "Architektin", 1, 1},
		{"Armin", "Gibs", "Stuntman", 1, 1},
		{"Nora", "Notfall", "Nachtschwester", 1, 1},
		{"Stella", "Standup", "Fitnesstrainerin", 1, 1}};
	
	int input = 0;
	
	printf("Hallo, wilkommen beim Event\n"); //Begrüßung3
	
	while(true)
	{
		printf("MENÜ:\n1 = Liste anzeigen\n2 = Liste\n3 = Program schließen\n");
		scanf("d", &input);
		switch(input)
		{
			case 1:
				for(int i = 0; i < 6;i++)
				{
					printf("%s | %s | %s\n", member[i].vorname, member[i].nachname, member[i].beruf);
				}
				break;
			case 2:
				break;
			case 3:
				exit(0);
				break;
			default:
				break;
		}
	}
}