//Rekursives Potenz und Summe rechnen
//Fabian Wwinhold
//prog_u04b

#include <stdio.h>

float power(float x, float y)
{
	float result = 0;				//rueckgabe ergebnis
	if(y == 0)
	{
		result = 1;					//endpunkt der rekursion (x^0=1)
	}
	else if(y < 0)					//negative potenz abfangen
	{
		result = 1 / power(x, -y);	//negatives umkehren
	}
	else
	{
		result = x * power(x, y-1);	//positive potenzen berechnen und in sich selber erweitern
	}
	
	return result;
}

float summe(float n, float i)
{
	if(n <= 0)						//ende der Schleife ist kleiner als der startindex
	{
		return 0;
	}
	
	if(i == n)						//Ende der Summen Schleife
	{
		return 2 * i + 1;			//Letzte Addition
	}
	else
	{
		return 2 * i + 1 + summe(n, i+1);	//nächste Addition zur aktuellen Addition
	}
}

int main()
{
	float x = 0;					//platzhalter der x eingabe
	float y = 0;					//platzhalter der y eingabe
	float n = 0;					//platzhalter der n eingabe
	float res = 0;					//platzhalter des ergebisses
	
	printf("Geben sie x y ein:\n");
	scanf("%f%f", &x, &y);			//Eingabe der x und y werte
	
	res = power(x, y);				//ergebnis berechnen
	printf("Das Ergebnis von %f^%f ist: %f\n", x, y, res);	//ergebnis ausgeben
	
	printf("Geben sie n ein:\n");
	scanf("%f", &n);				//eingabe des n Wertes
	
	res = summe(n, 1);					//ergebnis berechnen
	printf("Das Ergebnis der Summe von (2 * i + 1) von 1 bis %f ist: %f\n", n, res);	//ergebnis ausgeben
}