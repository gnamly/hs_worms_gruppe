//Primzahlen
//Fabian Wwinhold
//prog_u04b

#include <stdio.h>
#include <stdbool.h>

bool IsPrime(int candidate) { // Test whether the parameter is a prime number.
		
	if ((candidate & 1) == 0) {
	    if (candidate == 2)  {
		return true;
	    }
	    else {
		return false;
	    }
	}
	return false;
}
 
int main(void) {    //Write prime numbers between 0 and N
	printf("Bitte geben Sie N-Zahl für das Intervall ein: ");
	int n;	
	scanf("%d", &n);	
	printf("--- Primes between 0 and %d ---\n", n);
	
	for (int i = 0; i < 100; i++) {
	    bool prime = IsPrime(i);
	    if (prime)
		printf("Prime: %d", i);
	}
}