/* prog_u05b.c
 * Beschreibung : Übungsaufgabe u05b
 * Name         : Fabian Weinhold 
 * Datum        : 14.11.2017
*/
 
#include <stdio.h>
#include <math.h>
 
#define maxInt 2.147.483.647
 
int eingabe()
{
	char buffer[10];
	char c;
	int index = 0;
	int expo = 9;
	int result = 0;
	
	printf("Bitte geben sie Zahlen ein:\n");
	while(c != EOF || index > 10)
	{
		c = getchar();
		
		if(c >= '0' && c <= '9')
		{
			printf("c = %c\n", c);
			buffer[index] = c;
			index++;
		}
	}
	buffer[index] = '\0';
	
	for(int i = 0;i<10;i++)
	{
		if(buffer[i] == '\0')
		{
			break;
		}
		result += (buffer[i] - '0') * pow(10, expo);
		expo--;
	}
	
	return result;
}

int main()
{
	int output = 0;
	
	output = eingabe();
	
	printf("Ihre Zahl lautet: %i\n", output);
}