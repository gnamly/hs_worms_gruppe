/* hallo­welt.c 
 * Beschreibung : 
 * Name         : 
 * Datum        : 
 */ 
#include <stdio.h> 
int main(){ 
    printf("Hallo Welt!\n");
	printf("-----------------------------\n");
	for(int i = 0; i <= 30; i++)
	{
		printf("Die Zählervariable hat den Wert: %i\n", i);
		if((i%3) == 0)
		{
			printf("Die Zahl %i ist durch 3 teilbar\n", i);
		}
		if((i%2) == 0)
		{
			printf("Die Zahl %i ist gerade\n", i);
		}
		else
		{
			printf("Die Zahl %i ist nicht gerade\n", i);
		}
		printf("-----------------------------\n");
	}
    return 0; 
} 