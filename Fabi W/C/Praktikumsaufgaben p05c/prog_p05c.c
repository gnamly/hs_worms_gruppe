/* prog_p05c.c
 * Beschreibung : Praktikumsaufgabe p05c
 * Name         : Fabian Weinhold 
 * Datum        : 21.11.2017
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

bool isPalin(char * input, int index, int len)			//Rekursive Funktion zur palindromüberprüfung
{
	if(index < len)										//Solange das Wort nich nicht durchgearbeitet ist..
	{
		if(input[index] == input[len])					//Bisher ein Palindrom dann weiter suchen mit neuen Stellen
		{
			return isPalin(input, index+1, len-1);
		}
		else											//Bisher kein Palindrom dann gleich abbrechen
		{
			return false;
		}
	}
	else												//Ende erreicht dann passt es
	{
		return true;
	}
}

void rot13(char * input, char * output)					//Rotiert die ausgabe bei den kleinbuchstaben in output rein
{
	for(int i = 0;i < 1024;i++)							//alles durchgehen
	{
		if(input[i] >= 'a' && input[i] <= 'm')			//von a bis m +13
		{
			output[i] = input[i] + 13;
		}
		else if(input[i] >= 'n' && input[i] <= 'z')		//von n bis z -13
		{
			output[i] = input[i] -13;
		}
		else											//andere zeichen kopieren
		{
			output[i] = input[i];
		}
	}
}

int main()
{
	char input[1024];									//Platzhalter Array für die Eingabe
	char output[1024];									//Platzhalter für die Ausgabe
	
	printf("Bitte geben sie ein Palindrom ein:\n");
	scanf("%s", input);									//Eingabe einlesen
	
	int len = strlen(input);							//Länge für die Überprüfung abfragen
	
	if(isPalin(input, 0, len-1)==true)					//Ausgabe für true und False
		printf("Das wort ist ein Palindrom\n");
	else
		printf("Das Wort ist kein Palindrom\n");
	
	rot13(input, output);								//rotieren
	printf("Die Verschluesselung des Wortes %s lautet: %s\n",input , output);	//Verschlüsselung ausgeben
}