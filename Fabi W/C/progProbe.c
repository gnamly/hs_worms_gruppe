/*
Name: Fabian Weinhold
Datum: 09.01.2018
Programm: Prog
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv [])
{
  if(argc < 2)                          //wenn zu wenige parameter übergeben wurden dann abbrechen
  {
    printf("Zu wenige Parameter!");
    return 0;
  }
  
  float min = atof(argv[1]);           //alle platzhalter
  float max;
  float ges;
  float durch;
  
  for(int i = 1;i < argc;i++)         //alle parameter durchlaufen außer index 0, da immer 0.000
  {
	ges += atof(argv[i]);
    if(atof(argv[i]) < min)           //wenn kleiner dann neues minimum und gesamt aufaddieren
    {
      min = atof(argv[i]);
      continue;
    }
    if(atof(argv[i]) > max)           //wenn groeßer dann neues maximum und gesamt aufaddieren
    {
      max = atof(argv[i]);
      continue;
    }
  }

  durch = ges/(argc-1);                   //durchschnitt rechnen

  printf("Das Minimum ist %f\nDas Maximum ist %f\nDer Durchschnitt ist %f\n", min, max, durch);
  return 0;
}
