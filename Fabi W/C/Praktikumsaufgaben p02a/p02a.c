/* prog_p01a.c 
 * Beschreibung : Praktikumsaufgabe p02a
 * Name         : Fabian Weinhold 
 * Datum        : 24.10.2017
 */
 
 #include <stdio.h>
 
 int main()
 { 
	 int count = 0; 					//Gesamter Zaehler der eingaben
	 int countGross = 0; 				//Zähler der Grossen Buchstaben
	 int countKlein = 0; 				//Zähler der kleinen Buchstaben
	 int countNumbers = 0;				//Zähler der Zahlen
	 int countSonst = 0; 				//Zähler der Sonstigen Zeichen
	 char input1;						//Platzhalter für input stream eingabe
		 
	 do
	 {
		 input1 = getchar(); 		//Input Stream abfangen
		 putchar(input1); 				//Wieder ausgeben
		 count++; 					 	//Zaehler hochsetzten
		 switch(input1)              	//switch Abfrage der grossen/kleinen Buchstaben und zahlen
		 {
			 case 'A' :
			 case 'B' :
			 case 'C' :
			 case 'D' :
			 case 'E' :
			 case 'F' :
			 case 'G' :
			 case 'H' :
			 case 'I' :
			 case 'J' :
			 case 'K' :
			 case 'L' :
			 case 'M' :
			 case 'N' :
			 case 'O' :
			 case 'P' :
			 case 'Q' :
			 case 'R' :
			 case 'S' :
			 case 'T' :
			 case 'U' :
			 case 'V' :
			 case 'W' :
			 case 'X' :
			 case 'Y' :
			 case 'Z' :
				countGross++;			//Zähler fuer grosse hochsetzten
				break;
			 case 'a' :
			 case 'b' :
			 case 'c' :
			 case 'd' :
			 case 'e' :
			 case 'f' :
			 case 'g' :
			 case 'h' :
			 case 'i' :
			 case 'j' :
			 case 'k' :
			 case 'l' :
			 case 'm' :
			 case 'n' :
			 case 'o' :
			 case 'p' :
			 case 'q' :
			 case 'r' :
			 case 's' :
			 case 't' :
			 case 'u' :
			 case 'v' :
			 case 'w' :
			 case 'x' :
			 case 'y' :
			 case 'z' :
				countKlein++;			//Zähler für kleine hochzaehlen
				break;
			 case '0' :
			 case '1' :
			 case '2' :
			 case '3' :
			 case '4' :
			 case '5' :
			 case '6' :
			 case '7' :
			 case '8' :
			 case '9' :
				countNumbers++;			//Zähler für Ziffern hochzaehlen
				break;
			 default : 
				countSonst++;			//Sonstige zeichen zaehlen
				break;
			 
		 }
	 }while(input1 != EOF); 			//nach Strg + D fragen
		 
	 printf("Die Anzahl der Eingaben beträgt: %i\n", count); 				//gesamte Anzahl ausgeben
	 printf("Die Anzahl der Großbuchstaben beträgt: %i\n", countGross);		//anzahl grosser buchstaben ausgeben
	 printf("Die Anzahl der Kleinbuchstaben beträgt: %i\n", countKlein);	//anzahl kleiner buchstaben
	 printf("Die Anzahl der Ziffern beträgt: %i\n", countNumbers);			//anzahl ziffern ausgeben
	 printf("Die Anzahl der Sonstigen Zeichen beträgt: %i\n", countSonst);	//anzahl sontiger zeichen ausgeben
 }