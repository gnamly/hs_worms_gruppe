/* prog_p01a.c 
 * Beschreibung : Praktikumsaufgabe p02a
 * Name         : Fabian Weinhold 
 * Datum        : 24.10.2017
 */
 
 #include <stdio.h>
 
 int main()
 { 
	 int count = 0; 					//Gesamter Zaehler der eingaben
	 int countGross = 0; 				//Zähler der Grossen Buchstaben
	 int countKlein = 0; 				//Zähler der kleinen Buchstaben
	 int countNumbers = 0;				//Zähler der Zahlen
	 int countSonst = 0; 				//Zähler der Sonstigen Zeichen
	 char input1;						//Platzhalter für input stream eingabe
		 
	 do
	 {
		 input1 = getchar(); 		//Input Stream abfangen
		 putchar(input1); 				//Wieder ausgeben
		 count++; 					 	//Zaehler hochsetzten
		 if(input1 >= 'a' && input1 <= 'z')
		 {
			 countKlein++;
		 }
		 else if(input1 >= 'A' && input1 <= 'Z')
		 {
			 countGross++;
		 }
		 else if(input1 >= '0' && input1 <= '9')
		 {
			 countNumbers++;
		 }
		 else
		 {
			 countSonst++;
		 }
	 }while(input1 != EOF); 			//nach Strg + D fragen
		 
	 printf("Die Anzahl der Eingaben beträgt: %i\n", count); 				//gesamte Anzahl ausgeben
	 printf("Die Anzahl der Großbuchstaben beträgt: %i\n", countGross);		//anzahl grosser buchstaben ausgeben
	 printf("Die Anzahl der Kleinbuchstaben beträgt: %i\n", countKlein);	//anzahl kleiner buchstaben
	 printf("Die Anzahl der Ziffern beträgt: %i\n", countNumbers);			//anzahl ziffern ausgeben
	 printf("Die Anzahl der Sonstigen Zeichen beträgt: %i\n", countSonst);	//anzahl sontiger zeichen ausgeben
 }